# Frontend Mentor - Stats preview card component solution

This is a solution to the [Stats preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/stats-preview-card-component-8JqbgoU62). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size

### Screenshot
Screenshot of my solution :

![Screenshot of my integration](./design/screenshot_of_my_work.png)

### Links

- Live Site URL: [https://maximelebonniec-challenges.gitlab.io/stats-preview-card-component/](https://maximelebonniec-challenges.gitlab.io/stats-preview-card-component/)

## My process

### Built with

- HTML5
- CSS 3
- Flexbox
- CSS variables
- SCSS

### What I learned

I learned to use flexbox and SCSS. SCSS is new for me and it's my first project with it.

I also learned to put an image in a whole container.

### Continued development

I want to continue to learn SCSS and flexbox. I think this is very important to sucessufully integrate a mockup design.

## Author

- Website - [Maxime Le Bonniec](https://maximelebonniec.gitlab.io/portfolio/)
- Gitlab profile - [@MaximeLeBonniec](https://gitlab.com/MaximeLeBonniec/)
- Frontend Mentor profile - [@MaximeLeBonniec](https://www.frontendmentor.io/profile/MaximeLeBonniec)
- LinkedIn - [@MaximeLeBonniec](https://www.linkedin.com/in/maxime-le-bonniec/)
